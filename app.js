const express = require("express");
const port = 8080;

const app = express();

// Thank you Sondre Tofte :D
app.use("/", express.static(__dirname + "/public"));

// Not needed, but leaving it in. The express.static(...) does it all!
app.get("/", (req, res) => {
	res.sendFile(__dirname + "/public/", err => {
		if (err) console.log("Failed to serve HTML, error: ", err);
	});
});

const server = app.listen(port, () => console.log(`It's be runnin on port ${port}`));
