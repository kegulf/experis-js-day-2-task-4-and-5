**Experis Academy, Norway**

**Authors:**

- **Odd Martin Hansen**

# Task descriptions

### Task 4 - Setup a node server

Setup and run a node server with `http`

- Use `npm` to install `http`
- The response should return “Hello Node World”

### Task 5 - Setup an Express website

Setup a node app with `express`

Serve an index.html file

- The index file should have a `<h1>` tag with “Hello Node World!!” as the text and a `<p>` tag with text of your choice.

Add a JavaScript file

- Use `console.log()` to display “Hello From an Express Site”
- You will need to use the `sendFile()` function to server your index file

### Task 5 part 2 - Host it on Heruko (or other hosting service)

Host the application on Heroku or some other hosting site of your choice.

# Implementation notes

**Task4**:

The work for this task is all commits up to, and including, `Merge branch 'omhans/feat_task4_implementation'`

**Task 5**:

I deployed a static webpage with express instead of creating one trigger for the HTML and one for the JS file

I used the following line to serve the public folder:

`app.use("/", express.static(__dirname + "/public"));`

**Task 5 part 2**:

The application was deployed to Firebase, due to previous experience with this hosting provider.

Link for the project on Firebase: https://experis-js-day-2-task-4-and-5.web.app/
